
nomnomdata.engine.parameters
============================

.. automodule:: nomnomdata.engine.parameters
   :members:
   :exclude-members: CodeDialectType, SQLDialectType, EnumDisplayType


Additonal
---------

.. autoclass:: nomnomdata.engine.parameters.CodeDialectType
   :members:
   :undoc-members:

.. autoclass:: nomnomdata.engine.parameters.EnumDisplayType
   :members:
   :undoc-members:


nomnomdata.engine.connections
-----------------------------
.. py:module:: nomnomdata.engine.connections

.. autoclass:: nomnomdata.engine.components.Connection
   :members:
   :exclude-members: all_parameters, serialize


.. literalinclude:: ../../nomnomdata-engine/nomnomdata/engine/connections.py
    :language: python
    :lines: 4-
