Engine CLI
==========
Requires that run the engine main entry point

.. code-block:: python

  if __name__ == "__main__":
    engine.main()

.. click:: nomnomdata.engine.components.engine:_cli
  :prog: python engine.py
  :show-nested:
  :commands: run, dump-yaml
